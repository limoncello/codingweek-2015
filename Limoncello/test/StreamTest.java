/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import API.MainAPI;
import App.Controler;
import App.Main;
import Data.Data;
import Data.ModelParameter;
import Data.Tweet;
import JDBC.SQLiteJDBC;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author boixel
 */
public class StreamTest {
    private Boolean isStream, ok;
    
    public StreamTest() {
        ModelParameter parameter = ModelParameter.getInstance();
                
                Data modele = Data.getInstance();
                Controler controler = new Controler();
                
		for (int i = 0; i < 12; i++) {
			ModelParameter.getInstance().setFieldKept(true, i, true);
		}
		for (int i = 0; i < 5; i++) {
			ModelParameter.getInstance().setFieldKept(false, i, true);
		}
		ModelParameter.getInstance().setFilterField(true, Main.LANG, "fr, en");
                ModelParameter.getInstance().setVisibleConsole(false);
                ModelParameter.getInstance().setTimer(1);
                ModelParameter.getInstance().setNB_TWEETS(100);
               List<Tweet> tweets = new ArrayList<Tweet>();//modele.getTweets();
                
                //test Stream
                
                isStream = true;
                System.out.println("On récupère maintenant un stream avec comme filtres les mots Obama, Trump et ISIS et le hashtag #COP21 sur des tweets en anglais.\n Le stream dure une minute.");
                ModelParameter.getInstance().setFilterField(true, Main.TEXT,
				"Obama, Trump");
		ModelParameter.getInstance().setFilterField(true, Main.HASH,
				"COP21");
		ModelParameter.getInstance().setFilterField(true, Main.LANG, "en");

                new SQLiteJDBC().store(new MainAPI(isStream).extractData());
                System.out.println("On vérifie maintenant que les tweets récupérés contiennent l'un des mots ou le hashtag choisi.");
                tweets = modele.getTweets();
                String trump = "Trump";
                String cop21 = "#COP21";
                String obama = "Obama";
                Pattern p1 = Pattern.compile(obama);
                Pattern p2 = Pattern.compile(trump);
                Pattern p4 = Pattern.compile(cop21);
                ok = true;
                String text = "";
                String buffer = "";
                for(Tweet tweet : tweets) {
                    buffer = tweet.getBody();
                    int longueur = buffer.length();
                    for (int i = 0; i < longueur; i++)
                    {
                        
			if (buffer.charAt(i) == '*') {
                            text = text + "";
                        }else text = text + buffer.charAt(i);
                    }
                    Matcher m1 = p1.matcher(text);
                    Matcher m2 = p2.matcher(text);
                    Matcher m4 = p4.matcher(text);
                    
                    if(m1.find() || m2.find() || m4.find())
                    {
                        //System.out.println(text);
                    }else ok = false;
                }
                if(ok)
                {
                    System.out.println("Success!");
                }else System.out.println("Nul.");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void hello() {
     if(!ok)
         fail("Le stream n'a pas récupéré les bons tweets");
     }
}
