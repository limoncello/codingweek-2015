package JDBC.SQLrequests.Insert;
import Data.Tweet;

public class EndDecoratorTweet extends InsertTweetDecorator {
	public EndDecoratorTweet(InsertRequest Request, Tweet tweet)
    {
    	super(Request, tweet);
    }
    
    // We take the previous request and add the author request to it.
    public String getSql(Tweet tweet)
    {
    	String temp = InsertRequest.getSql(tweet);
    	String str = temp.substring(0, temp.length()-1);
    	return str+");";
    
    }
}
