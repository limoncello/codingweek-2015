package JDBC.SQLrequests.Insert;

import Data.Tweet;

public class ImageDecoratorTweet extends InsertTweetDecorator {

	public ImageDecoratorTweet(InsertRequest Request, Tweet tweet) {
		super(Request, tweet);
	}

	// We take the previous request and add the new request to it.
	public String getSql(Tweet tweet) {
		if (tweet.getMediasPath() != null) {
			String[] tabpath = tweet.getMediasPath();
			String previous = InsertRequest.getSql(tweet);
			int i = 0;
			String temp = "";
			for (i = 0; i < tabpath.length; i++) {
				temp = temp + "INSERT INTO IMAGES VALUES ('" + tweet.getId()
						+ "', '" + tabpath[i] + "');";
			}
			return previous + temp;
		} else
			return "";
	}
}