package Graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import loadAndSave.Load;

public class LoadWindow extends JFrame {

	JFileChooser fileChooser;

	public LoadWindow() {
		this.setSize(400, 400);
		this.setVisible(true);
		new File("Save").mkdir();
		new File("Save/SaveDB").mkdir();
		// chooser.setControlButtonsAreShown(false);

		this.fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("./Save/SaveDB"));
		this.add(fileChooser);

		fileChooser.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				int returnValue = fileChooser.showOpenDialog(null);
				if (ae.getActionCommand().equals(fileChooser.APPROVE_SELECTION)) {
					String selectedFile = fileChooser.getSelectedFile()
							.getName();
					new Load(selectedFile);
					System.out.println(selectedFile);
				} else if (ae.getActionCommand().equals(
						fileChooser.CANCEL_SELECTION)) {

				}

			}
		});
	}
}
