package Graphic;

import java.awt.BorderLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FiltreUnitPanel extends JPanel {
	private JCheckBox checkBox;
	private JTextField textField;

	public void setEnabled(boolean b) {
		this.checkBox.setEnabled(b);
		this.textField.setEnabled(b);
	}

	public FiltreUnitPanel(JCheckBox c, JTextField t) {
		super(new BorderLayout());

		this.checkBox = c;
		this.add(checkBox, BorderLayout.WEST);
		if (t != null) {
			this.textField = t;
			this.add(textField, BorderLayout.EAST);

		}

	}

	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public JTextField getTextField() {
		return textField;
	}

}
